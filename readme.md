# Opgave Orders basic oefening #

Vanaf deze oefening starten met het bouwen van een ordersysteem dat telkens uitgebreider zal worden door extra functionaliteit die we toevoegen. De opbouw van dit ordersysteem zal op dezelfde manier verlopen als er in de theorielessen (hoofdstuk 7) een systeem wordt ontwikkeld voor personenbeheer. 

Het meest basissysteem van orderbeheer in deze oefening start van de klassen Order en OrderList. Een order bevat de volgende attributen: orderNumber, productName, numberOfUnits, customerName. In de interfaces OrderIf en OrderListIf vind je de specifieke vereisten. Jullie zullen ook merken dat in deze twee klassen een methode *giveOverview* moet voorzien worden. Zoals in de theorielessen besproken, geeft deze methode een overzicht van alle gegevens in een object of lijst in Stringvorm.

Vanaf deze oefening gebruiken we ook de **structuur** die we in de **theorielessen** aanleren en die tot goed gestructureerde grotere applicaties zal leiden. Dit wil zeggen dat we de opsplitsing maken tussen *zelfstandig naamwoordklassen (nouns) en werkwoordklassen (verbs)*. De noun klassen voor deze applicatie zijn al in de vorige alinea besproken. Voor de verb klassen hebben we nood aan: een Reader en een Searcher klasse.

De **OrderFileReader** laat ons toe een csv-bestand met orders in te lezen. Een voorbeeld-bestand is beschikbaar in de resources directory van de repository. Vanaf nu zetten we alle csv-bestanden die we inlezen bij het begin van het uitvoeren van een applicatie in deze folder. De OrderFileReader is een technische klasse, waarvan niet alle techniciteit van gekend moet zijn. Gebruik het voorbeeld van in het handboek van PersonFileReader (codevoorbeeld 21 p. 151) en pas dit aan naar zodat het orders kan lezen in plaats van personen.

Met de **OrderListSearcher** kunnen we opzoekingen doen in een OrderList. Maak deze Searcher analoog aan de PersonListSearcher uit het handboek (codevoorbeeld 26 p. 153). Voorzie methoden om te zoeken op meer dan een aantal units per order, en op een specifieke klant-naam.

Ten slotte willen we melden dat vanaf deze oefening de **App.java** klasse de naam van een Mirrorklasse zal krijgen. Dit is om ook in lijn te zijn met de gebruikte terminologie van het handboek. In deze oefening zal er dus een **OrderMirror** klasse zijn.

Een overzicht van al deze vereisten kan bekeken worden in onderstaand domeinklassendiagram.

![klassendiagram_Orders-basic](img/Klassendiagram_orders-basic.png)


## Functionaliteit van applicatie ##

De code dient de volgende functionaliteit te voldoen/voorzien (deze functionaliteit moeten jullie voorzien in de klassen en vervolgens zelf een keer uitvoeren in de **App.java** klasse):

* Enkele instanties aanmaken van de klassen "Order"
* In het command prompt weergeven dat deze instanties zijn aangemaakt
* Een orderlijst aanmaken en de aangemaakte orders hieraan toevoegen
* Een overzicht van deze orderlijst printen in de command line
* Een OrderListSearcher aanmaken en hierop een zoekopdracht uitvoeren
* De resultaten van de zoekopdracht printen


## Leerdoelen van oefening ##

* De structuur van software uit de theorielessen kunnen toepassen in de praktijk
* Een Reader-klasse schrijven
* Een Searcher-klasse schrijven
* Een Mirror (App.java) opstellen


## Mogelijke problemen bij het lezen van een csv-bestand ##

Wanneer een csv-bestand gelezen moet worden dat zich in een andere directory (folder) bevindt, kunnen er zich problemen voordoen wanneer er zich spaties in het pad bevinden van het csv-bestand. Bijvoorbeeld: /mijn folder/main/resources

Wanneer er zich problemen voordoen bij het inlezen van het bestand, controleer dan of dit bij jou het geval is. We kunnen dit probleem namelijk oplossen door lijn 10 in codevoorbeeld 21 p. 151 te vervangen door de code:

// Remove possible empty spaces from path:
String decodedPath = URLDecoder.decode(getClass().getClassLoader().getResource(aName + ".csv").toString(), "UTF-8");
			
// Get the file that needs to be read:
URL urlPath = new URL(decodedPath);
File aFile = new File(urlPath.getFile());
			