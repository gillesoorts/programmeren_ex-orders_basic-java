package be.uantwerpen;
import java.util.*;

// Let op voor het examen, er is een verschil tussen OrderList en een array of orders

public class OrderList implements OrderListIf {
    private ArrayList<Order> members = new ArrayList();
    private String orderListName;

    public OrderList(String orderListName){
        this.orderListName = orderListName;
    }

    public ArrayList<Order> getMembers(){
        return this.members = members;
    }

    public void addMember(Order order){
        members.add(order);
    }

    public void addAllMembers(OrderList orderList){
       for(Order member: orderList.getMembers()){
           members.add(member);
       }
    }
}