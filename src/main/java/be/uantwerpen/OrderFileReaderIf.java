package be.uantwerpen;

public interface OrderFileReaderIf {
    public OrderList readCsvFile(String aName);
}