package be.uantwerpen;
import java.io.*;
import java.util.*;

public class Order implements OrderIf {
// Een order bevat de volgende attributen: orderNumber, productName, numberOfUnits, customerName.
//een methode *giveOverview*
private int orderNumber;
private String productName;
private int numberOfUnits;
private String customerName;

public Order(int orderNumber, String productName, int numberOfUnits, String customerName){
        this.orderNumber = orderNumber;
        this.productName = productName;
        this.numberOfUnits = numberOfUnits;
        this.customerName = customerName;
}
    public int getOrderNumber(){
        return this.orderNumber;
    }
    
    public void setOrderNumber(int orderNumber){
        this.orderNumber = orderNumber;
    }
    
    public String getProductName(){
        return productName;
    }
    
    public void setProductName(String productName){
        this.productName = productName;
    }
    
    public int getNumberOfUnits(){
        return numberOfUnits;
    }
    
    public void setNumberOfUnits(int numberOfUnits){
        this.numberOfUnits = numberOfUnits;
    }
    
    public String getCustomerName(){
        return customerName;
    }
    
    public void setCustomerName(String customerName){
        this.customerName = customerName;
    }
    
    public String giveOverview(){
        // als dit errors geeft, kijk naar this.getordernumber en number of units omdat dat ints zouden moeten zijn ipv strings
        String output = "OrderNumber: " + this.getOrderNumber() + ", productName: " + this.getProductName() + ", numberOfUnits: " + this.getNumberOfUnits() + ", customerName: " + this.getCustomerName();
        return output;

    }
}