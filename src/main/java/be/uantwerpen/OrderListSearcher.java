package be.uantwerpen;
import java.util.*;
import java.io.*;


public class OrderListSearcher implements OrderListSearcherIf {
    private OrderList orders; 

    public OrderListSearcher(OrderList orders){
        this.orders = orders;
    };

    public OrderList searchNumberOfUnitsGreaterThan(int limit){
        OrderList grotebestellingen = new OrderList("grotebestellingen");
        for(Order member: orders.getMembers()){
            if(member.getNumberOfUnits()>= limit){
               grotebestellingen.addMember(member);
            }
        }
        return grotebestellingen;
    }

    public OrderList searchCustomerNameEqualTo(String name){
        OrderList bestellingenvanklant = new OrderList("bestellingenvanklant");
        for(Order member: orders.getMembers()){
            if(member.getCustomerName() == name){
               bestellingenvanklant.addMember(member);
            }
        }
        return bestellingenvanklant;
    }

}