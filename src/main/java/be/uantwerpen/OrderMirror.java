package be.uantwerpen;

public class OrderMirror {
    public static void main(String[] args) {
        Order order1 = new Order(1, "pennen", 3, "Doreen");
        Order order2 = new Order(2, "stiften", 7, "Jens");

        System.out.println("Alles is in orde, de bestellingen van "+ order1.getCustomerName() + " en " + order2.getCustomerName() + " zijn aangemaakt");
        OrderList lijst1 = new OrderList("lijst");
        lijst1.addMember(order1);
        lijst1.addMember(order2);

        for(Order members: lijst1.getMembers()){
            System.out.println(members.giveOverview());
        }

        OrderListSearcher test = new OrderListSearcher(lijst1);
        OrderList resultaat = test.searchNumberOfUnitsGreaterThan(5);

        for(Order members: resultaat.getMembers()){
            System.out.println("Dit order vraagt meer dan 5 units: " + members.giveOverview());
        }
    }
}
