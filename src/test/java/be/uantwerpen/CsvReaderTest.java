package be.uantwerpen;

import org.junit.Assert;
import org.junit.Test;

public class CsvReaderTest {
    @Test
    public void testCsvReader() {
        OrderFileReader reader = new OrderFileReader();
        OrderList orders = reader.readCsvFile("orders");
        Assert.assertEquals(3, orders.getMembers().size());
    }
}