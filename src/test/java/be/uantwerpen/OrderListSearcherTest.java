package be.uantwerpen;

import org.junit.Assert;
import org.junit.Test;

public class OrderListSearcherTest {
    @Test
    public void testNumberOfUnitsGreaterThan() {
        OrderList orders = new OrderList("Programming Fuel");
        Order pizza = new Order(5452, "Calzone", 5, "Philip");
        orders.addMember(pizza);
        Order beer = new Order(1337, "Bud Light", 1, "Philip");
        orders.addMember(beer);
        Order pastis = new Order(7515, "Pastis de Beilem", 4578, "Gilles");
        orders.addMember(pastis);

        OrderListSearcher searcher = new OrderListSearcher(orders);
        OrderList result = searcher.searchNumberOfUnitsGreaterThan(3);

        Assert.assertEquals(2, result.getMembers().size());
    }

    @Test
    public void testSearchCustomerNameEqualTo() {
        OrderList orders = new OrderList("Programming Fuel");
        Order pizza = new Order(5452, "Calzone", 5, "Philip");
        orders.addMember(pizza);
        Order beer = new Order(1337, "Bud Light", 1, "Philip");
        orders.addMember(beer);
        Order pastis = new Order(7515, "Pastis de Beilem", 4578, "Gilles");
        orders.addMember(pastis);

        OrderListSearcher searcher = new OrderListSearcher(orders);
        OrderList result = searcher.searchCustomerNameEqualTo("Philip");

        Assert.assertEquals(2, result.getMembers().size());
    }

}