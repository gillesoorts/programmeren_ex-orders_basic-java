package be.uantwerpen;

import org.junit.Assert;
import org.junit.Test;

public class OrderTest {
    @Test
    public void testOverview() {
        Order order = new Order(5452, "Calzone", 5, "Philip");
        Assert.assertEquals("OrderNumber: 5452, productName: Calzone, numberOfUnits: 5, customerName: Philip", order.giveOverview());
    }
}